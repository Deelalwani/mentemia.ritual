import Layout from "./components/Layout";
import { BrowserRouter as Router, Route } from "react-router-dom";
import TeamOverview from "./components/steps/TeamOverview"
import {ThemeProvider} from '@material-ui/core/styles';
import theme from "../src/styling/theme"

function App() {
  return (
    <ThemeProvider theme={theme}>
    <Layout>
      <Router>
        <Route path="/" component={TeamOverview} />
      </Router>
    </Layout>
    </ThemeProvider>
  );
}

export default App;
