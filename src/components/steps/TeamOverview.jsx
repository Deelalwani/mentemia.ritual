import { makeStyles, Typography } from '@material-ui/core';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import React from "react";
import styled from 'styled-components';
import { colors } from '../../styling/styles/colors';
import { Button } from "../Button";
import { Link } from '../Link';
import { Modal } from "../Modal"


const RootDiv = styled.div`
text-align: center;
margin: 0 20%;
`

const ButtonDiv = styled.div`
display: flex;
padding-left: 35%;
align-items: baseline;
`

const useStyles = makeStyles((theme) => ({
    button: {
        marginTop: theme.spacing(10),
        marginRight: "5vh",
    },
    link: {
        color: colors.royalBlue,
        fontFamily: 'Averta-Semibold',
        fontWeight: 500,
    },
    linkButton: {
        margin: theme.spacing(1),
    },
    paper: {
        position: 'absolute',
        width: "384px",
        height: "429px",
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        flexDirection: "column",
        textAlign: "center",
        borderRadius: "50px",
        top: "50%",
        left: "50%",
        transform: "translate(-51%, -55%)"
    },
}));


const TeamOverview = () => {
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const body = (
        <div className={classes.paper}>
            <HelpOutlineIcon color={"primary"} fontSize={"200"} />
            <Typography variant="h2" component="h2" gutterBottom>
                Add a new team
            </Typography>
            <Typography variant="body2" gutterBottom>
                Do this if you haven't yet created your team and recorded your team's rituals.
            </Typography>
            <Typography variant="body2" gutterBottom>
                If you've already created your team, view or update your team's rituals via the unique link that would've been emailed to you.
            </Typography>
        </div>
    ); 

    return (
    <RootDiv>
            <Typography variant="h1" component="h1" gutterBottom>
                NZ POST
            </Typography>
            <Typography variant="body2" gutterBottom>
                A simple way to bake wellbeing into your workplace is to create rituals for team wellbeing. The idea is to link a wellbeing action, like group deep breathing, to something in your work day (a trigger), such as a regular meeting. In this way, wellbeing becomes an automatic part of every day.
            </Typography>
                <ButtonDiv>
                    <Button className={classes.button}> Add a new team</Button>
                    <Link style={{ padding: 0 }} startIcon={<HelpOutlineIcon color={"primary"} />} className={classes.linkButton}>
                        <Typography
                            variant="h4"
                            className={classes.link}
                            onClick={handleOpen}>
                            Help
                        </Typography>
                    </Link>
                </ButtonDiv>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    {body}
                </Modal>
        </RootDiv>  
            ) 
} 

export default TeamOverview;